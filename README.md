# Contents

- [Repository links of deployed services](#used-services)
- [Intruction how to run docker-compose](#intruction-how-to-use-docker-compose)
- [Example deployment in Azure for Thailand demo](#user-content-example-deployment-in-azure-for-thailand-demo-with-7-tenants)
- [References](#references)

## Used services 

Here is the list of services started with docker-compose, every service has documentation in it's own repository.

* [Transfer service](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer)
* [Authorization service](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management)
* [Nsi webservice](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse)
* [Keycloak](https://www.keycloak.org/)
* [data-explorer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-viewer)
* [data-viewer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-viewer)
* [data-lifecycle-manager](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager)
* [sdmx-faceted-search](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search)
* [share](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-share)
* [config](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config)
* [proxy](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy)

## Intruction how to use docker-compose

`NB!` The prerequisite of running commands below is installed **docker** & **docker-compose**. The installation of these clients is out of scope of this manual, see the [references](#references) in the end of this document for addition information.

> Current configuration in the repository for running services under localhost, where every service is accessible on the dedicated port.
> Running services in the localhost doesn't require a proxy service, as it assumes usage of DNS.
> Manual bellow covers what changes in place are needed to configure custom setup with your domain names and tenants.
> In localhost only **default** tenant can be used.

1. Edit [front/config/tenants.json](front/config/tenants.json) to update tenants list. Tenant is a configuration unit that enables customization of UI and visible datasouces in DE/DLM. 

2. Edit [front/config/datasouces.json](front/config/datasouces.json) to update NSI service datasources. This list can include both external and internal (started with the docker-compose) NSI web services. The datasources ID defined in the list are references from DE,DLM,SFS etc. 

3. In Azure configure static public IP and enable incoming traffic at least at port `80` (That will be used by a proxy service that will route traffic to other services) . Then configure DNS with you domain to point to this static IP. Wildcard A record can be setup for the flexibility.
Then routing should be configured at the proxy level by editing routes.json config file. 
In the [front/proxy/routes.json](front/proxy/routes.json) is an example for [Thailand demo](#user-content-example-deployment-in-azure-for-thailand-demo-with-7-tenants). In the config file **host** uses regex against domain name in the request and if matches it redirects
traffic to internal service defined under **target**

4. Change docker-compose environemnt variables. In example bellow indicated changes that were done for [demo cloud](#user-content-example-deployment-in-azure-for-thailand-demo-with-7-tenants) deployment:

[/front/.env](front/.env)
 - KEYCLOAK_URL => http://keycloak.siscc.org
 - TRANSFER_URL => http://transfer.app.siscc.org/1.2
 - SHARE_URL => http://share.app.siscc.org

[/back/.env](back/.env)
 - KEYCLOAK_URL => http://keycloak.siscc.org

5. The longest step is to configure front end application for every tenant. 
In the current repository there is an example of **default** tenant used only in localhost and 7 more tenants (demo, thailand etc)
used in the [demo cloud](#user-content-example-deployment-in-azure-for-thailand-demo-with-7-tenants). `Note!` that when application is supposed to be run
behind proxy service urls in configuration can be relative, and when without proxy they should be absolute.

> You can take [front/configs/demo](front/configs/demo) as an example tenant and do the following changes to it:

[Data explorer config](front/configs/demo/data-explorer/settings.json)
  - sdmx.datasourceIds -> used datasouces from the [front/config/datasouces.json](front/config/datasouces.json) list
  - search.endpoint -> search service url
  - share.confirmUrl -> url of data viewer for confirmation
  - share.endpoint -> url of share service for chart submission
  
[DLM config](/front/configs/demo/data-lifecycle-manager/settings.json)
  - sdmx.datasourceIds -> used datasouces from the [front/config/datasouces.json](front/config/datasouces.json) list
  - sdmx.datasources -> same list as above, configure here `dataExplorerUrl` and `backgroundColor`. When `isExternal` is true upload of artifacts will be disabled. 
  
[Data viewer config](/front/configs/demo/data-viewer/settings.json)
  - share.endpoint -> Url of share service  

> Copy demo folder and repeat actions above to as many tenants as you will need in your setup.

More information on customization of DE can be found with links bellow:

- https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-customisation/
- https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-configuration/

6. The last step is to configure Look & feel of tenant UI: styles & logo images. It's up to the requirements but all tenants can use the same style and logos or it can be dedicated set of css/images for every tenant. Here structure and files should be
created like was configured in **settings.json** file of corresponding tenant application. (DE, DLM, Viewer). In this repository every tenant has it's own css/images resources in 
[front/assets](front/assets) folder.

When all configuration is done you can start containers with a docker-compose command like following:

From the **back** folder start backend services:

`sudo docker-compose -f backend-demo.yml -f keycloak-postgres.yml up -d`

From the **front** folder start frontend services:

`sudo docker-compose -f docker-demo.yml -f proxy.yml up -d`

If the SSL/TLS certificate is not setup and services are run for the first time it's required to disable Keycloak SSL in order to access main admin panel. For 
default realm (siscc) it's already disabled. In order to disable SSL, one needs to connect to **Keycloak** container and execute following commands:

`NB!` You will be asked for Keycloak's admin password. Use one defined in [back/.env](back/.env) environment file in **KEYCLOAK_PWD** variable

```
sudo docker-compose -f keycloak-postgres.yml exec keycloak bash
cd keycloak/bin
./kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user admin
./kcadm.sh update realms/master -s sslRequired=NONE
```
 
## Example deployment in Azure for Thailand demo with 7 tenants

The example demo setup runs on Helmes Azure subscription (oecdstat). The VM name is **DotstatUbuntuDocker** in the **statV8** resource group, with following resource *Standard D4s v3 (4 vcpus, 16 GiB memory)*

### Back

- [NSI cambodia](http://cambodia.nsi.siscc.org)
- [NSI ethiopia](http://ethiopia.nsi.siscc.org)
- [NSI ghana](http://ghana.nsi.siscc.org)
- [NSI kyrgyzstan](http://kyrgyzstan.nsi.siscc.org)
- [NSI tanzania](http://tanzania.nsi.siscc.org)
- [NSI thailand](http://thailand.nsi.siscc.org)
- [NSI demo](http://demo.nsi.siscc.org)
- [Transfer service](http://transfer.app.siscc.org/swagger)
- [Auth service](http://auth.app.siscc.org/swagger)
- [Keycloak](http://keycloak.siscc.org/auth)

### Front

- [SFS](http://sfs.app.siscc.org/healthcheck)
- [Share](http://share.app.siscc.org/healthcheck)

Demo|Cambodia|Ethiopia|Ghana|Kyrgyzstan|Tanzania|Thailand
---|---|---|---|---|---|---
[DLM](http://demo.dlm.app.siscc.org)|[DLM](http://cambodia.dlm.app.siscc.org)|[DLM](http://ethiopia.dlm.app.siscc.org)|[DLM](http://ghana.dlm.app.siscc.org)|[DLM](http://kyrgyzstan.dlm.app.siscc.org)|[DLM](http://tanzania.dlm.app.siscc.org)|[DLM](http://thailand.dlm.app.siscc.org)
[DE](http://demo.de.app.siscc.org)|[DE](http://cambodia.de.app.siscc.org)|[DE](http://ethiopia.de.app.siscc.org)|[DE](http://ghana.de.app.siscc.org)|[DE](http://kyrgyzstan.de.app.siscc.org)|[DE](http://tanzania.de.app.siscc.org)|[DE](http://thailand.de.app.siscc.org)
[Viewer](http://demo.viewer.app.siscc.org/api/healthcheck)|[Viewer](http://cambodia.viewer.app.siscc.org/api/healthcheck)|[Viewer](http://ethiopia.viewer.app.siscc.org/api/healthcheck)|[Viewer](http://ghana.viewer.app.siscc.org/api/healthcheck)|[Viewer](http://kyrgyzstan.viewer.app.siscc.org/api/healthcheck)|[Viewer](http://tanzania.viewer.app.siscc.org/api/healthcheck)|[Viewer](http://thailand.viewer.app.siscc.org/api/healthcheck)

## References

- [docker install manual](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [docker-compose install manual](https://docs.docker.com/compose/install/)
